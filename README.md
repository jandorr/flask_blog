# README #

Flask tutorial from Digital Ocean.

### Run The App ###
To run this web application, tell Flask where to find it with the FLASK_APP environment variable:

`export FLASK_APP=app`

Set the FLASK_ENV environment variable (DO NOT USE "development" IN PRODUCTION!!!):

`export FLASK_ENV=development`

Lastly, run the application using the flask run command:

`flask run`

### Before You Push ###
Run `isort foo.py`, `black foo.py` and `pylint foo.py`.

### Useful Links ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* [Digital Ocean Tutorial](https://www.digitalocean.com/community/tutorials/how-to-make-a-web-application-using-flask-in-python-3)
* [Flask-Login](https://flask-login.readthedocs.io/en/latest/)
* [Flask-SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/)
* [Virtual environments](https://flask.palletsprojects.com/en/1.1.x/installation/#virtual-environments)

### TODO ###

* Implement Flask-Login
* Implement Flask-SQLAlchemy
* pytest this project ([tutorial](https://flask.palletsprojects.com/en/1.1.x/testing/))
