"""Flask blog app."""

import os
import sqlite3

from flask import Flask, flash, redirect, render_template, request, url_for
from werkzeug.exceptions import abort


def get_db_connection():
    """Return an open connection to our SQLite database file database.db.

    Returns:
        Connection: Open Connection instance.

    """
    conn = sqlite3.connect("database.db")
    # Make rows behave like regular Python dictionaries.
    conn.row_factory = sqlite3.Row
    return conn


def get_post(post_id):
    """Get post with given post ID.

    Args:
        post_id (int): ID of post to get.

    Returns:
        dict: The found post.

    Raises:
        HTTPException: 404 if not found.

    """
    conn = get_db_connection()
    cursor = conn.execute("SELECT * FROM posts WHERE id = ?", (post_id,))
    found_post = cursor.fetchone()
    conn.close()

    if found_post is None:
        abort(404)

    return found_post


# __name__ is used to tell the instance where it’s located.
# You need this because Flask sets up some paths behind the scenes.
app = Flask(__name__)
app.config["SECRET_KEY"] = os.getenv("SECRET_KEY") or "my$ecRetk@y"


@app.route("/")
def index():
    """Fetch and render all posts to index.html template."""
    conn = get_db_connection()
    posts = conn.execute("SELECT * FROM posts").fetchall()
    conn.close()
    return render_template("index.html", posts=posts)


@app.route("/<int:post_id>")
def post(post_id):
    """Get post with ID from URL and render post.html template."""
    found_post = get_post(post_id)
    return render_template("post.html", post=found_post)


@app.route("/create", methods=("GET", "POST"))
def create():
    """Create post route handler.

    If request method is GET, render create.html template.
    If request method is POST, get the title and content from the
    request form, validate title, push to the database and redirect to
    index.html.

    """
    if request.method == "POST":
        title = request.form["title"]
        content = request.form["content"]

        if not title:
            flash("Title is required")
        else:
            conn = get_db_connection()
            sql_cmd = "INSERT INTO posts (title, content) VALUES (?, ?)"
            conn.execute(sql_cmd, (title, content))
            conn.commit()
            conn.close()
            return redirect(url_for("index"))

    return render_template("create.html")


@app.route("/<int:post_id>/edit", methods=("GET", "POST"))
def edit(post_id):
    """Edit post route handler.

    If request method is GET, get the post with ID post_id and render
    template edit.html.
    If request method is POST, validate request form fields , update
    post from database and redirect to index.html.

    Args:
        post_id (int): The post ID to edit.

    """
    found_post = get_post(post_id)

    if request.method == "POST":
        title = request.form["title"]
        content = request.form["content"]

        if not title:
            flash("Title is required!")
        else:
            conn = get_db_connection()
            conn.execute(
                "UPDATE posts SET title = ?, content = ? WHERE id = ?",
                (title, content, post_id),
            )
            conn.commit()
            conn.close()
            return redirect(url_for("index"))

    return render_template("edit.html", post=found_post)


@app.route("/<int:post_id>/delete", methods=("POST",))
def delete(post_id):
    """Delete post route handler.

    Delete post with ID from URL, set the message to flash and redirect
    to index.html.

    Args:
        post_id (int): The post ID to delete.

    """
    found_post = get_post(post_id)
    conn = get_db_connection()
    conn.execute("DELETE FROM posts WHERE id = ?", (post_id,))
    conn.commit()
    conn.close()
    flash('"{}" was successfully deleted!'.format(found_post["title"]))
    return redirect(url_for("index"))
