"""Initialize database."""

import os
import sqlite3


def main():
    """Create database.db file and execute schema.sql. If database.db
    already exists, return.
    """

    if os.path.isfile("database.db"):
        print("database.db already exists")
        return

    connection = sqlite3.connect("database.db")

    with open("schema.sql") as f:
        connection.executescript(f.read())

    cur = connection.cursor()

    cur.execute(
        "INSERT INTO posts (title, content) VALUES (?, ?)",
        ("First Post", "foo"),
    )

    cur.execute(
        "INSERT INTO posts (title, content) VALUES (?, ?)",
        ("Second Post", "bar"),
    )

    connection.commit()
    connection.close()


if __name__ == "__main__":
    main()
